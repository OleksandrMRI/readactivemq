package ua.shpp.activemqfinal;

import java.util.List;

public class ErrorPojo {
    List<String> errors;

    public List<String> getErrors() {
        return errors;
    }

    public ErrorPojo setErrors(List<String> errors) {
        this.errors = errors;
        return this;
    }
}
