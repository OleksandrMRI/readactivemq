package ua.shpp.activemqfinal;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.PropertiesReader;

import javax.jms.*;
import java.util.List;
import java.util.Properties;

public class ActiveMQProducer {
    private static final Logger log = LoggerFactory.getLogger(ActiveMQProducer.class);
    private static final String OUTSIDE_PROPERTIES = "config.properties";
    private static final String INSIDE_PROPERTIES = "conf/config.properties";

    public static void main(String[] args) {
        int n = 0;
        if (args.length > 0) {
            try {
                n = Integer.parseInt(args[0]);
                log.info("N POJOs: {}", n);
            } catch (NumberFormatException e) {
                log.error("NumberFormatException system variable could not cast to int: ", e);
                System.exit(1);
            }
        } else {
            log.error("There is no system variables:");
            System.exit(1);
        }
        produceToActiveMq(n);
    }

    public static void produceToActiveMq(int n) {
        PropertiesReader propertiesReader = new PropertiesReader(new Properties(), OUTSIDE_PROPERTIES, INSIDE_PROPERTIES);
        String user = propertiesReader.getProperty("userOut");
        String pass = propertiesReader.getProperty("passOut");
        String port = propertiesReader.getProperty("portOut");

        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(user, pass, port);
        factory.setTrustedPackages(List.of("ua.shpp.activemqfinal"));

        try {
            Connection connection = factory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination queue = session.createQueue(propertiesReader.getProperty("outQueue"));
            MessageProducer messageProd = session.createProducer(queue);

            log.info("1 пачка старт");
            long time = Integer.parseInt(propertiesReader.getProperty("time"));
            new Sender(session, messageProd).sendMessages(time, n);
            log.info("1 пачка фініш");

            log.info("поізон старт");
            messageProd.send(session.createObjectMessage(new Pojo().setName(propertiesReader.getProperty("poisonPill"))));
            log.info("поізон фініш");

            messageProd.close();
            session.close();
            connection.close();
        } catch (JMSException e) {
            log.error("JMSException: {}", e.getMessage());
        }
    }
}
