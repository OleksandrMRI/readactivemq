package ua.shpp.activemqfinal;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Pojo implements Serializable {

    @Size(min = 7, message = "Length of name should be >=7.")
    @Pattern(regexp = ".*[a]+.*", message = "Name should include one or more letter 'a'.")
    private String name;

    @Check(value=3,message = "Field should not divide by 3 with integer result.")
    @Min(value = 10, message = "Count should be  equals or more than 10.")
    private int count;

    private LocalDateTime created_at;

    public String getName() {
        return name;
    }

    public Pojo setName(String name) {
        this.name = name;
        return this;
    }

    public int getCount() {
        return count;
    }

    public Pojo setCount(int count) {
        this.count = count;
        return this;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public Pojo setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pojo pojo = (Pojo) o;
        return count == pojo.count && Objects.equals(name, pojo.name) && Objects.equals(created_at, pojo.created_at);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, count, created_at);
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", created_at='" + created_at + '\'' +
                "}\n";
    }
}
