package ua.shpp.activemqfinal;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.PropertiesReader;

import javax.jms.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class ActiveMQReceiver {
    private static final String OUTSIDE_PROPERTIES = "config.properties";
    private static final String INSIDE_PROPERTIES = "conf/config.properties";
    private static final Logger log = LoggerFactory.getLogger(ActiveMQReceiver.class);

    public static void main(String[] args) {
        PropertiesReader propertiesReader = new PropertiesReader(new Properties(), OUTSIDE_PROPERTIES, INSIDE_PROPERTIES);
        receiveFromActiveMq(propertiesReader);
    }

    public static void receiveFromActiveMq(PropertiesReader propertiesReader) {
        String user = propertiesReader.getProperty("userIn");
        String pass = propertiesReader.getProperty("passIn");
        String port = propertiesReader.getProperty("portIn");

        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(user, pass, port);
        factory.setTrustAllPackages(true);
        String fileForInvalidPojo = propertiesReader.getProperty("invalid");
        String fileForValidPojo = propertiesReader.getProperty("valid");
        try (FileWriter invalid = new FileWriter(fileForInvalidPojo, true);
             FileWriter valid = new FileWriter(fileForValidPojo, true);
             CSVPrinter invalidCsvPrinter = new CSVPrinter(invalid, CSVFormat.newFormat(';'));
             CSVPrinter validCsvPrinter = new CSVPrinter(valid, CSVFormat.newFormat(';'))) {
            invalid.write("name;count;error\n");
            valid.write("name;count\n");

            Connection connection = factory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            log.debug("queue: {}", propertiesReader.getProperty("inQueue"));
            Destination queue = session.createQueue(propertiesReader.getProperty("inQueue"));
            MessageConsumer messageConsumer = session.createConsumer(queue);


            log.info("File invalid exists: {}", new File(fileForInvalidPojo).exists());
            log.info("File valid exists: {}", new File(fileForValidPojo).exists());

            String poison = propertiesReader.getProperty("poisonPill");
            FilePrinter invalidFilePrinter = new FilePrinter(invalidCsvPrinter);
            FilePrinter validFilePrinter = new FilePrinter(validCsvPrinter);
            new Consumer(invalidFilePrinter, validFilePrinter, messageConsumer).consumeMessages(poison);
            messageConsumer.close();
            session.close();
            connection.close();
        } catch (JMSException e) {
            log.error("JMSException: {}", e.getMessage());
        } catch (IOException e) {
            log.error("IOException, file to print is not exist: ", e);
        }
    }
}
