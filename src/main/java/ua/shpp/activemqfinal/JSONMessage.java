package ua.shpp.activemqfinal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JSONMessage {
    private String stringMapper;
    private static final Logger log = LoggerFactory.getLogger(JSONMessage.class);


    public String createJson(Pojo pojo) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        try {
            stringMapper = mapper.writeValueAsString(pojo);
//            log.info(stringMapper);
        } catch (JsonProcessingException e) {
            log.error("There is no JSON structure", e);
        }
        return stringMapper;
    }
}
