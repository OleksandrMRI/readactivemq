package ua.shpp.activemqfinal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.stream.Stream;

public class Sender {
    Session session;
    MessageProducer messageProd;
    private final Logger log = LoggerFactory.getLogger(Sender.class);

    public Sender(Session session, MessageProducer messageProd) {
        this.session = session;
        this.messageProd = messageProd;
    }

    public void sendMessages(long duration, int n) {
        Random rand = new Random();
        LocalDateTime prodDuration = LocalDateTime.now().plusSeconds(duration);
        getStream(n, rand, prodDuration)
                .forEach(x -> {
                    try {
                        ObjectMessage objectMessage = session.createObjectMessage(x);
                        messageProd.send(objectMessage);
                    } catch (JMSException e) {
                        log.error("JMSException: ", e);
                    }
                });

    }

    private Stream<Pojo> getStream(int n, Random rand, LocalDateTime prodDuration) {
        return Stream.generate(() -> new Pojo().setName(new Name().createName(rand)).setCount(rand.nextInt(1000)).
                        setCreated_at(LocalDateTime.now())).
                takeWhile(x -> LocalDateTime.now().isBefore(prodDuration)).limit(n);
    }
}
