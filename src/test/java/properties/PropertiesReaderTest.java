package properties;

import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PropertiesReaderTest {
    PropertiesReader pr;

    @Test
    void getPropertyCheckOneLine() {
        PropertiesReader propertiesReader = new PropertiesReader(new Properties(), "bla", "conf/test.properties");
        assertAll(
                () -> assertEquals("3", propertiesReader.getProperty("time")),
                () -> assertEquals("meme", propertiesReader.getProperty("outQueue")),
                () -> assertEquals("mumu", propertiesReader.getProperty("inQueue")),
                () -> assertEquals("pill", propertiesReader.getProperty("poisonPill"))
        );
    }
}