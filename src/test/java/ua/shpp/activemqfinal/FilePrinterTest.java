package ua.shpp.activemqfinal;


import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class FilePrinterTest {

//    private final Logger log = LoggerFactory.getLogger(FilePrinterTest.class);
    @Mock
    CSVPrinter csvPrinterMock;
    @InjectMocks
    FilePrinter filePrinter;

    @Test
    void printPojoCheckWriteToFile() throws IOException {
        //given
        Pojo pojo = new Pojo().setName("eirutrth").setCount(10);
        //execute
        filePrinter.printPojo(pojo);
        //assertions
        Mockito.verify(csvPrinterMock, times(1)).printRecord(eq("eirutrth"), eq("10\n"));
    }

    @Test
    void printPojoCheckWriteToFile1Test() throws IOException {
        //given
        Pojo pojo = new Pojo().setName("eirutrth").setCount(10);
        CSVPrinter csvPrinterMock = Mockito.mock(CSVPrinter.class);
        //execute
        new FilePrinter(csvPrinterMock).printPojo(pojo);
        //assertions
        Mockito.verify(csvPrinterMock, times(1)).printRecord(eq("eirutrth"), eq("10\n"));
    }
}