package ua.shpp.activemqfinal;

import jakarta.validation.ConstraintViolation;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConsumerTest {
    @Mock
    FilePrinter filePrinter;
    @Mock
    MessageConsumer messageConsumerMock;
    @Mock
    ViolationPojo violationPojo;
    @Mock
    Set<ConstraintViolation<Pojo>> violations;
    @InjectMocks
    Consumer consumer;

    @Test
    void consumeMessagesCheckArgumentValidFilePrinter() throws JMSException, IOException {
        //given
        String poison = "babababa";
        ActiveMQObjectMessage activeMQObjectMessage = new ActiveMQObjectMessage();
        Pojo pojo = new Pojo().setName("babababa").setCount(16);
        activeMQObjectMessage.setObject(pojo);
        Mockito.when(messageConsumerMock.receive(anyLong())).thenReturn(activeMQObjectMessage);
        //execute
        consumer.consumeMessages(poison);
        //assertion
        Mockito.verify(filePrinter, never()).printPojo(new Pojo().setName("babababa").setCount(16), violations);
        Mockito.verify(filePrinter, times(1)).printPojo(new Pojo().setName("babababa").setCount(16));
    }

    @Test
    void consumeMessagesCheckArgumentInalidFilePrinter() throws JMSException, IOException {
        //given
        String poison = "babaa";
        ActiveMQObjectMessage activeMQObjectMessage = new ActiveMQObjectMessage();
        Pojo pojo = new Pojo().setName("babaa").setCount(15);
        activeMQObjectMessage.setObject(pojo);
        Mockito.when(messageConsumerMock.receive(anyLong())).thenReturn(activeMQObjectMessage);
        ViolationPojo violationPojo = new ViolationPojo();
        //execute
        consumer.consumeMessages(poison);
        //assertion
        Mockito.verify(filePrinter, times(1))
                .printPojo(new Pojo().setName("babaa").setCount(15), violationPojo.getViolation(pojo));
        Mockito.verify(filePrinter, never()).printPojo(new Pojo().setName("babaa").setCount(15));
    }

    @Test
    void consumeMessages1() throws JMSException, IOException {
        //given
        String poison = "baba";
        ActiveMQObjectMessage activeMQObjectMessage = new ActiveMQObjectMessage();
        Pojo pojo = new Pojo().setName("baba");
        activeMQObjectMessage.setObject(pojo);
        Mockito.when(messageConsumerMock.receive(anyLong())).thenReturn(activeMQObjectMessage);
        //execute
        consumer.consumeMessages(poison);
        //assertion
        Mockito.verify(violationPojo, never()).getViolation(pojo);
    }

    @Test
    void receiveFromActiveMqDoNothingVerifyRepositoryCallTest() throws IOException, JMSException {
        String poison = "baba";
        consumer.consumeMessages(poison);
        verify(messageConsumerMock, times(1)).receive(1000);
    }
}