package ua.shpp.activemqfinal;


import jakarta.validation.ConstraintViolation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ViolationPojoTest {
    private ViolationPojo violationPojo;

    @BeforeEach
    void setUp() {
        violationPojo = new ViolationPojo();
    }

    @Test
    void getViolationTestCountValidation() {
        Pojo pojo = new Pojo().setName("KAkaruka").setCount(3).setCreated_at(LocalDateTime.now());
        List<String> collection = violationPojo.getViolation(pojo).stream().map(v -> v.getMessage()).collect(Collectors.toList());
        assertThat(collection, hasItem("Count should be  equals or more than 10."));
    }

    @Test
    void getViolationTestNameLengthValidation() {
        Pojo pojo = new Pojo().setName("Kkrua").setCount(15).setCreated_at(LocalDateTime.now());
        List<String> collection = violationPojo.getViolation(pojo).stream().map(v -> v.getMessage()).collect(Collectors.toList());
        assertThat(collection, hasItem("Length of name should be >=7."));
    }

    @Test
    void getViolationTestLetterAPresentValidation() {
        Pojo pojo = new Pojo().setName("Kkruhrut").setCount(15).setCreated_at(LocalDateTime.now());
        List<String> collection = violationPojo.getViolation(pojo).stream().map(v -> v.getMessage()).collect(Collectors.toList());
        assertThat(collection, hasItem("Name should include one or more letter 'a'."));
    }

    @Test
    void getViolationTestNumber7PresentValidation() {
        Pojo pojo = new Pojo().setName("Kkruhrut").setCount(15).setCreated_at(LocalDateTime.now());
        List<String> collection = violationPojo.getViolation(pojo).stream().map(v -> v.getMessage()).collect(Collectors.toList());
        assertThat(collection, hasItems("Name should include one or more letter 'a'.", "Field should not divide by 3 with integer result."));
    }
    @Test
    void getViolation() {
        ValidatorPojo vp = new ValidatorPojo();
        Pojo pojo = new Pojo().setName("Kul").setCount(20).setCreated_at(LocalDateTime.now());
        Set<ConstraintViolation<Pojo>> violations=vp.getViolation(pojo);
        assertEquals(2, violations.size());
    }
}