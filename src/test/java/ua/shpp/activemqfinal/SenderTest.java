package ua.shpp.activemqfinal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.MessageProducer;
import javax.jms.Session;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
@ExtendWith(MockitoExtension.class)
class SenderTest {

    @Test
    void sendMessagesArgumentVerify() {
        Sender send = Mockito.mock(Sender.class);
        doNothing().when(send).sendMessages(anyLong(),anyInt());
        send.sendMessages(2,1);
        Mockito.verify(send).sendMessages(2,1);
    }

    @Mock
    Session session;
    @Mock
    MessageProducer messageProd;
    @InjectMocks
    Sender sender;

    @Test
    void getStreamSize() throws Exception {
        int number = 3;
        LocalDateTime duration = LocalDateTime.now().plusSeconds(1);
        Object[] params = {number,new Random(), duration};
        assertEquals(3,
                ((Stream<Pojo>)this.invokePrivateMethod(sender, "getStream", params)).count());
        assertEquals(new Pojo().getClass(),
                ((Stream<Pojo>)this.invokePrivateMethod(sender, "getStream", params))
                        .findFirst().get().getClass());
        assertNotNull(((Stream<Pojo>)this.invokePrivateMethod(sender, "getStream", params))
                        .findFirst().get());
    }

    private Object invokePrivateMethod (Object test, String methodName, Object params[]) throws Exception {
        Object ret = null;
        final Method[] methods =
                test.getClass().getDeclaredMethods();
        for (int i = 0; i < methods.length; ++i) {
            if (methods[i].getName().equals(methodName)) {
                methods[i].setAccessible(true);
                ret = methods[i].invoke(test, params);
                break;
            }
        }
        return ret;
    }
}